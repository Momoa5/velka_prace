# Velká práce

Jedná se o projekt v Pythonu (Django) do předmětu SKJ. Cílem tohoto projektu je vytvořit software pro atletický oddíl. Tento projekt umožňuje správu uživatelů, manipulaci s kurzy, disciplínami, závody, oceněními a spoustu dalších. Pro lepší výsledky - Pylint byl použit nástroj Black.
