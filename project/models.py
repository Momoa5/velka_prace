"""System module."""
from django.db import models


class Course(models.Model):
    cid = models.AutoField(
        db_column="cID", primary_key=True
    )  # Field name made lowercase.
    name = models.CharField(max_length=40)
    place = models.CharField(max_length=40)
    date = models.DateTimeField()
    description = models.CharField(max_length=100, blank=True, null=True)
    typeoflicense = models.CharField(
        db_column="typeOfLicense", max_length=40
    )  # Field name made lowercase.
    validuntil = models.DateField(db_column="validUntil")  # Field name made lowercase.
    pid = models.ForeignKey(
        "Payment", models.DO_NOTHING, db_column="pID", blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "Course"


class Discipline(models.Model):
    did = models.AutoField(
        db_column="dID", primary_key=True
    )  # Field name made lowercase.
    name = models.CharField(max_length=30)

    class Meta:
        managed = False
        db_table = "Discipline"


class Email(models.Model):
    emid = models.AutoField(
        db_column="emID", primary_key=True
    )  # Field name made lowercase.
    email = models.CharField(max_length=20, blank=True, null=True)
    text = models.CharField(max_length=200, blank=True, null=True)

    class Meta:
        managed = False
        db_table = "Email"


class Event(models.Model):
    eid = models.AutoField(
        db_column="eID", primary_key=True
    )  # Field name made lowercase.
    name = models.CharField(
        db_column="Name", max_length=50
    )  # Field name made lowercase.
    place = models.CharField(max_length=40)
    eventdate = models.DateField(db_column="eventDate")  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "Event"


class Participateonevent(models.Model):
    eid = models.OneToOneField(
        Event, models.DO_NOTHING, db_column="eID", primary_key=True
    )  # Field name made lowercase.
    uid = models.ForeignKey(
        "Users", models.DO_NOTHING, db_column="uID"
    )  # Field name made lowercase.
    performance = models.IntegerField()
    did = models.ForeignKey(
        Discipline, models.DO_NOTHING, db_column="dID"
    )  # Field name made lowercase.
    poeid = models.AutoField(db_column="poeID")  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "ParticipateOnEvent"
        unique_together = (("eid", "uid", "poeid"),)


class Payment(models.Model):
    pid = models.AutoField(
        db_column="pID", primary_key=True
    )  # Field name made lowercase.
    name = models.CharField(
        db_column="Name", max_length=50
    )  # Field name made lowercase.
    type = models.SmallIntegerField()
    exposuredate = models.DateTimeField(
        db_column="exposureDate"
    )  # Field name made lowercase.
    quantity = models.IntegerField(blank=True, null=True)
    price = models.IntegerField()
    reason = models.CharField(max_length=100, blank=True, null=True)
    bossid = models.ForeignKey(
        "Users", models.DO_NOTHING, db_column="bossID"
    )  # Field name made lowercase.
    cid = models.ForeignKey(
        Course, models.DO_NOTHING, db_column="cID", blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "Payment"


class Takecourse(models.Model):
    uid = models.OneToOneField(
        "Users", models.DO_NOTHING, db_column="uID", primary_key=True
    )  # Field name made lowercase.
    cid = models.ForeignKey(
        Course, models.DO_NOTHING, db_column="cID"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "TakeCourse"
        unique_together = (("uid", "cid"),)


class Usercategory(models.Model):
    ucid = models.AutoField(
        db_column="ucID", primary_key=True
    )  # Field name made lowercase.
    categoryname = models.CharField(
        db_column="categoryName", max_length=10
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "UserCategory"


class Users(models.Model):
    uid = models.AutoField(
        db_column="uID", primary_key=True
    )  # Field name made lowercase.
    firstname = models.CharField(
        db_column="firstName", max_length=20
    )  # Field name made lowercase.
    lastname = models.CharField(
        db_column="lastName", max_length=20
    )  # Field name made lowercase.
    login = models.CharField(max_length=20)
    address = models.CharField(max_length=40)
    telephone = models.CharField(max_length=12)
    bankaccount = models.CharField(
        db_column="bankAccount", max_length=30, blank=True, null=True
    )  # Field name made lowercase.
    email = models.CharField(max_length=20)
    birthday = models.DateField()
    lastvisit = models.DateTimeField(
        db_column="lastVisit", blank=True, null=True
    )  # Field name made lowercase.
    type = models.CharField(
        db_column="Type", max_length=10
    )  # Field name made lowercase.
    isactive = models.BooleanField(db_column="isActive")  # Field name made lowercase.
    trainerid = models.ForeignKey(
        "self", models.DO_NOTHING, db_column="trainerID", blank=True, null=True
    )  # Field name made lowercase.
    ucid = models.ForeignKey(
        Usercategory, models.DO_NOTHING, db_column="ucID"
    )  # Field name made lowercase.
    password = models.CharField(max_length=20)

    class Meta:
        managed = False
        db_table = "Users"


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=150)

    class Meta:
        managed = False
        db_table = "auth_group"


class AuthGroupPermissions(models.Model):
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey("AuthPermission", models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = "auth_group_permissions"
        unique_together = (("group", "permission"),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey("DjangoContentType", models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = "auth_permission"
        unique_together = (("content_type", "codename"),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.BooleanField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=150)
    email = models.CharField(max_length=254)
    is_staff = models.BooleanField()
    is_active = models.BooleanField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = "auth_user"


class AuthUserGroups(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = "auth_user_groups"
        unique_together = (("user", "group"),)


class AuthUserUserPermissions(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = "auth_user_user_permissions"
        unique_together = (("user", "permission"),)


class Awarded(models.Model):
    uid = models.OneToOneField(
        Users, models.DO_NOTHING, db_column="uID", primary_key=True
    )  # Field name made lowercase.
    aid = models.ForeignKey(
        "Awardshistory", models.DO_NOTHING, db_column="aID"
    )  # Field name made lowercase.
    reason = models.CharField(max_length=200, blank=True, null=True)
    awid = models.AutoField(db_column="awID")  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "awarded"
        unique_together = (("uid", "aid", "awid"),)


class Awardshistory(models.Model):
    aid = models.AutoField(
        db_column="aID", primary_key=True
    )  # Field name made lowercase.
    date = models.DateField()
    place = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = "awardsHistory"


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.SmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey(
        "DjangoContentType", models.DO_NOTHING, blank=True, null=True
    )
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = "django_admin_log"


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = "django_content_type"
        unique_together = (("app_label", "model"),)


class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = "django_migrations"


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = "django_session"


class Ispaying(models.Model):
    uid = models.OneToOneField(
        Users, models.DO_NOTHING, db_column="uID", primary_key=True
    )  # Field name made lowercase.
    pid = models.ForeignKey(
        Payment, models.DO_NOTHING, db_column="pID"
    )  # Field name made lowercase.
    paymentdate = models.DateField(
        db_column="paymentDate", blank=True, null=True
    )  # Field name made lowercase.
    ispaid = models.BooleanField(db_column="isPaid")  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "isPaying"
        unique_together = (("uid", "pid"),)
