"""athleticsWeb URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
import athletics.views as views

#pridavame cesty na views
urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index, name='index'),
    path('users/', views.users, name='users'),
    path('courses/', views.courses, name='courses'),
    path('courses/<int:cid>', views.courses, name='courses'),
    path('awardEvents/', views.awardEvents, name='awardEvents'),
    path('awardEvents/awarded<int:aid>', views.awarded, name='awarded'),
    path('races/', views.races, name='races'),
    path('settings/disciplines/', views.disciplines, name='disciplines'),
    path('settings/categories/', views.userCategories, name='categories'),
    path('settings/', views.settings, name='settings'),
    path('users/addUser', views.addUser, name='addUser'),
    path('courses/addCourse', views.addCourse, name='addCourse'),
    path('races/addRace', views.addRace, name='addRace'),
    path('awardEvents/addAwardEvent', views.addAwardEvent, name='addAwardEvent'),
    path('settings/disciplines/addDiscipline', views.addDiscipline, name='addDiscipline'),
    path('users/deleteUser<int:uid>', views.deleteUser, name='deleteUser'),
    path('races/raceResults<int:eid>', views.raceResults, name='raceResults'),
]
