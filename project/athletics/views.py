"""System module."""
from django.shortcuts import render, redirect
from django.http import HttpResponse, Http404
from athletics.models import (
    Users,
    Course,
    Awardshistory,
    Event,
    Discipline,
    Usercategory,
    Participateonevent,
    Takecourse,
    Awarded,
)
from athletics.forms import BirthdaySeachForm


# ---------- MOJE REAL APP----------#
def index(request):
    Users_count = Users.objects.all().count()
    context = {"users_count": Users_count}

    return render(request, "index.html", context=context)


def users(request):
    UsersAll = Users.objects.all()
    if request.method == "POST":
        if "Žák" in request.POST:
            cat = Usercategory.objects.get(categoryname="Žák")
            UsersAll = Users.objects.filter(ucid=cat.ucid)

        if "Dorost" in request.POST:
            cat = Usercategory.objects.get(categoryname="Dorost")
            UsersAll = Users.objects.filter(ucid=cat.ucid)

        if "Junior" in request.POST:
            cat = Usercategory.objects.get(categoryname="Junior")
            UsersAll = Users.objects.filter(ucid=cat.ucid)

        if "Dospělý" in request.POST:
            cat = Usercategory.objects.get(categoryname="Dospělý")
            UsersAll = Users.objects.filter(ucid=cat.ucid)

        if "Vše" in request.POST:
            UsersAll = Users.objects.all()

        if "Aktivní" in request.POST:
            UsersAll = Users.objects.filter(isactive=True)

        if "Neaktivní" in request.POST:
            UsersAll = Users.objects.filter(isactive=False)

    context = {"users": UsersAll}

    return render(request, "users.html", context=context)


def addUser(request):
    if request.method == "POST":
        firstname_g = request.POST["firstName"]
        lastname_g = request.POST["lastName"]
        login_g = request.POST["login"]
        address_g = request.POST["address"]
        telephone_g = request.POST["telephone"]
        email_g = request.POST["email"]
        birthday_g = request.POST["birthday"]
        type_g = request.POST["typeSelect"]
        category_id = request.POST["categorySelect"]
        category_g = Usercategory.objects.get(pk=category_id)

        user = Users(
            firstname=firstname_g,
            lastname=lastname_g,
            login=login_g,
            address=address_g,
            telephone=telephone_g,
            bankaccount=None,
            email=email_g,
            birthday=birthday_g,
            lastvisit=None,
            type=type_g,
            isactive=True,
            trainerid=None,
            ucid=category_g,
        )
        user.save()

        return redirect("/users/")

    return render(request, "addUser.html")


def deleteUser(request, uid):
    user = Users.objects.get(pk=uid)

    if request.method == "POST":
        if "yes" in request.POST:
            user.isactive = False
            user.save()
            return redirect("users")

        elif "no" in request.POST:
            return redirect("users")

    context = {"user": user}

    return render(request, "deleteUser.html", context=context)


def courses(request, cid=""):
    CoursesAll = Course.objects.all()

    if request.method == "POST":
        try:
            user = Users.objects.get(pk=12)
            course = Course.objects.get(pk=cid)
            tc = Takecourse(uid=user, cid=course)
            tc.save()

        except:
            return Http404("Už jste zaregistrován")

    context = {"courses": CoursesAll}

    return render(request, "courses.html", context=context)


def addCourse(request):
    if request.method == "POST":
        name_g = request.POST["name"]
        place_g = request.POST["place"]
        date_g = request.POST["date"]
        description_g = request.POST["description"]
        if description_g == "None":
            description_g = None
        licenceType_g = request.POST["licenceTypeSelect"]
        validUntil_g = request.POST["validUntil"]

        course = Course(
            name=name_g,
            place=place_g,
            date=date_g,
            description=description_g,
            typeoflicense=licenceType_g,
            validuntil=validUntil_g,
            cid=None,
        )
        course.save()

        return redirect("/courses/")

    return render(request, "addCourse.html")


def awardEvents(request):
    events = Awardshistory.objects.all()
    context = {"awardEvents": events}

    return render(request, "awardEvents.html", context=context)


def addAwardEvent(request):
    if request.method == "POST":
        place_g = request.POST["place"]
        date_g = request.POST["date"]
        ae = Awardshistory(date=date_g, place=place_g)
        ae.save()

        return redirect("/awardEvents/")

    return render(request, "addAwardEvent.html")


def awarded(request, aid):
    a = Awarded.objects.filter(aid=aid)
    context = {"aa": a}

    return render(request, "awarded.html", context=context)


def races(request):
    races = Event.objects.all()
    context = {"races": races}

    return render(request, "races.html", context=context)


def addRace(request):
    if request.method == "POST":
        name_g = request.POST["name"]
        place_g = request.POST["place"]
        eventDate_g = request.POST["date"]

        race = Event(name=name_g, place=place_g, eventdate=eventDate_g)
        race.save()

        return redirect("/races/")

    return render(request, "addRace.html")


def disciplines(request):
    disciplinesAll = Discipline.objects.all()
    context = {"disciplines": disciplinesAll}

    return render(request, "disciplines.html", context=context)


def addDiscipline(request):
    if request.method == "POST":
        name_g = request.POST["name"]

        discipline = Discipline(name=name_g)
        discipline.save()

        return redirect("settings/disciplines/")

    return render(request, "addDiscipline.html")


def userCategories(request):
    categories = Usercategory.objects.all()
    context = {"categories": categories}

    return render(request, "categories.html", context=context)


def settings(request):
    user = Users.objects.get(pk=2)
    context = {"user": user}
    return render(request, "settings.html", context=context)


def raceResults(request, eid):
    poe = Participateonevent.objects.filter(eid=eid)
    context = {"poe": poe}

    return render(request, "raceResults.html", context=context)
