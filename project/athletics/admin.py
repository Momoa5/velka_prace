from django.contrib import admin
from athletics.models import Users, Course, Awardshistory, Event, Discipline, Usercategory, Participateonevent, Takecourse, Awarded

admin.site.register(Users)
admin.site.register(Course)
admin.site.register(Awardshistory)
admin.site.register(Event)
admin.site.register(Discipline)
admin.site.register(Usercategory)
admin.site.register(Participateonevent)
admin.site.register(Takecourse)
admin.site.register(Awarded)
