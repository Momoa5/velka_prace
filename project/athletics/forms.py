from django import forms


class BirthdaySeachForm(forms.Form):
    from_birthday = forms.DateField(label='from')
    to_birthday = forms.DateField(label='to')